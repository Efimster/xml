pub const LT:u8 = "<".as_bytes()[0];
pub const RT:u8 = ">".as_bytes()[0];
pub const SLASH:u8 = "/".as_bytes()[0];
pub const EQ:u8 = "=".as_bytes()[0];
pub const WHITESPACE:u8 = 0x20;
pub const NEW_LINE:u8 = 0xd;
pub const CARET_RETURN:u8 = 0xa;
pub const TAB:u8 = 0x9;
pub const QUESTION_MARK:u8 = "?".as_bytes()[0];
pub const QUOTE:u8 = "\"".as_bytes()[0];
pub const SINGLE_QUOTE:u8 = "'".as_bytes()[0];
pub const ESCAPE:u8 = "\\".as_bytes()[0];

pub const WHITESPACE_SET:&[u8] = &[WHITESPACE, NEW_LINE, CARET_RETURN, TAB];
pub const QUOTE_SET:&[u8] = &[QUOTE, SINGLE_QUOTE];
pub const TAG_SEPARATORS_SET:&[u8] = &[WHITESPACE, NEW_LINE, TAB, CARET_RETURN, RT, "!".as_bytes()[0], QUESTION_MARK];

pub const CDATA_TAG:&str = "![CDATA[";
pub const CDATA_TAG_BYTES:&[u8] = CDATA_TAG.as_bytes();
pub const CDATA_END:&str = "]]>";
pub const CDATA_END_BYTES:&[u8] = CDATA_END.as_bytes();
pub const COMMENT_TAG:&str = "!--";
pub const COMMENT_TAG_BYTES:&[u8] = COMMENT_TAG.as_bytes();
pub const COMMENT_END:&str = "-->";
pub const COMMENT_END_BYTES:&[u8] = COMMENT_END.as_bytes();
pub const P_INSTRUCTIONS_TAG_BYTES:&[u8] = "?".as_bytes();
pub const P_INSTRUCTIONS_END_BYTES:&[u8] = "?>".as_bytes();