use crate::structs::attribute::Attribute;
use crate::enums::xml_error::XmlError;
use std::io::BufRead;
use crate::utils::constants::{WHITESPACE_SET, QUOTE_SET};
use slib::utils::parse_util;
use std::io;

use super::constants::*;

pub const QUOTE_OR_WHITESPACE:&[u8] = &[QUOTE, SINGLE_QUOTE, WHITESPACE, NEW_LINE, CARET_RETURN, TAB];
pub const EQ_OR_WHITESPACE:&[u8] = &[EQ, WHITESPACE, NEW_LINE, CARET_RETURN, TAB];

pub fn parse_attributes(buf:&mut impl BufRead) -> Result<Vec<Attribute>, XmlError>{
    let mut to = Vec::new();
    let mut result = Vec::new();
    loop {
        match parse_util::read_while_any(buf, WHITESPACE_SET, &mut to) {
            Err(err) if matches!(err.kind(), io::ErrorKind::UnexpectedEof)  => break,
            Err(err) => return Err(err.into()),
            Ok((found, _)) => {
                to.truncate(0);
                to.push(found);
            }
        }

        match parse_util::read_until_any(buf, EQ_OR_WHITESPACE, &mut to) {
            Err(err) if matches!(err.kind(), io::ErrorKind::UnexpectedEof)  => (),
            Err(err) => return Err(err.into()),
            Ok((sep, _)) if sep == EQ => {
                let (sep, _) = parse_util::read_until_any(buf, QUOTE_SET, &mut to)?;
                buf.read_until(sep, &mut to)?;
            },
            Ok(_) => ()
        }
        
        let attr_str = std::str::from_utf8(&to)?;
        let attr = attr_str.parse()?;

        result.push(attr);
    }

    Ok(result)
}

pub fn to_attribute_string<T:ToString>(key:T, value:T) -> String {
    format!("{}={}", key.to_string(), value.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_attributes() -> Result<(), XmlError> {
        let mut string = "  xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/fact-footnote\" \
        xlink:from=\"F_000343\" \
        xlink:to=\"FNT_000001\" \
        xlink:type=\"arc\"  html ".as_bytes();
        let attributes = parse_attributes(&mut string)?;
        assert_eq!(attributes.len(), 5);
        assert_eq!(&attributes[0].name, "xlink:arcrole");
        assert_eq!(&attributes[0].value, "http://www.xbrl.org/2003/arcrole/fact-footnote");
        assert_eq!(&attributes[1].name, "xlink:from");
        assert_eq!(&attributes[1].value, "F_000343");
        assert_eq!(&attributes[2].name, "xlink:to");
        assert_eq!(&attributes[2].value, "FNT_000001");
        assert_eq!(&attributes[3].name, "xlink:type");
        assert_eq!(&attributes[3].value, "arc");
        assert_eq!(&attributes[4].name, "html");
        assert_eq!(&attributes[4].value, "");

        Ok(())
    }

    #[test]
    fn test_parse_attributes_whitespace() -> Result<(), XmlError> {
        let mut string = "  attr=\"a b\"\nattr2='a \" b' ".as_bytes();
        let attributes = parse_attributes(&mut string)?;
        assert_eq!(attributes.len(), 2);
        assert_eq!(&attributes[0].name, "attr");
        assert_eq!(&attributes[0].value, "a b");
        assert_eq!(&attributes[1].name, "attr2");
        assert_eq!(&attributes[1].value, "a \" b");

        Ok(())
    }
}