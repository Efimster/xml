use std::str::FromStr;
use crate::enums::xml_error::{XmlError, XmlErrorKind};
use core::fmt;
use std::fmt::{Formatter, Display};

pub struct Attribute {
    pub name:String,
    pub value:String,
}

impl Attribute {
    pub fn new<T:ToString>(name:T, value:T) -> Self {
        Attribute {name:name.to_string(), value:value.to_string()}
    }
}

impl FromStr for Attribute {
    type Err = XmlError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut pattern = s.splitn(2, "=");
        let name = pattern.next().ok_or(XmlErrorKind::Parse)?.trim();
        let value = match pattern.next() {
            Some(value) => {
                let value = value.trim();
                &value[1 .. value.len() - 1]
            },
            None => "",
        };
        Ok(Attribute::new(name, value))
    }
}

impl fmt::Display for Attribute {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}={}", self.name, self.value)
    }
}

impl fmt::Debug for Attribute {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_attribute_from_str() -> Result<(), XmlError> {
        let string = "  aaa=\"bbb\" ";
        let attribute = Attribute::from_str(&string).unwrap();
        assert_eq!(&attribute.name, "aaa");
        assert_eq!(&attribute.value, "bbb");

        Ok(())
    }
}