use std::collections::HashMap;
use crate::structs::attribute::Attribute;
use std::collections::hash_map::{IntoIter, Iter};

#[derive(Clone, Debug, PartialEq)]
pub struct AttributeList(HashMap<String, String>);

impl<'a> IntoIterator for &'a AttributeList {
    type Item = (&'a String, &'a String);
    type IntoIter = Iter<'a, String, String>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl IntoIterator for AttributeList {
    type Item = (String, String);
    type IntoIter = IntoIter<String, String>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<T> From<T> for AttributeList
    where T:IntoIterator<Item=Attribute>
{
    fn from(attributes: T) -> Self {
        let mut result = AttributeList::new();
        for attr in attributes {
            result.add_attribute(attr);
        }
        result
    }
}

impl AttributeList {
    pub fn new() -> AttributeList {
        AttributeList(HashMap::new())
    }

    pub fn add<T:ToString>(&mut self, key:T, value:T) -> Option<String> {
        self.0.insert(key.to_string(), value.to_string())
    }

    pub fn add_attribute(&mut self, attribute:Attribute) -> Option<String> {
        self.0.insert(attribute.name, attribute.value)
    }

    pub fn iter(&self) -> Iter<String, String> {
        self.0.iter()
    }

    pub fn get(&self, key:&str) -> Option<&String> {
        self.0.get(key)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}