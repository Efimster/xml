use core::fmt;
use std::fmt::{Formatter, Display};
use crate::enums::xml_error::XmlError;
use std::io::BufRead;
use crate::enums::node::Node;

#[derive(PartialEq)]
pub enum NodeList {
    None,
    XmlNodes(Vec<Node>),
    UnParsed(String),
}

impl NodeList {
    pub fn from_reader(reader:&mut impl BufRead) -> Result<Self, XmlError> {
        let mut nodes = Vec::new();
        loop{
            match Node::from_reader(reader) {
                Ok(node) => {
                    nodes.push(node)
                },
                Err(err) if err.is_eof() => {
                    break;
                },
                Err(err) => return Err(err),
            }
        }
        let result = if nodes.len() == 0 {
            NodeList::None
        }
        else {
            NodeList::XmlNodes(nodes)
        };
        Ok(result)
    }

    pub fn nodes(&self) -> &[Node] {
        match self {
            Self::None => &[],
            Self::XmlNodes(nodes) => nodes,
            Self::UnParsed(_) => panic!("nodes need to be parsed first"),
        }
    }

    pub fn nodes_mut(&mut self) -> &mut [Node] {
        match self {
            Self::None => &mut [],
            Self::XmlNodes(nodes) => nodes,
            Self::UnParsed(_) => panic!("nodes need to be parsed first"),
        }
    }

    pub fn resolve(&mut self) -> Result<(), XmlError> {
        match self {
            Self::UnParsed(string) => {
                let mut string = string.as_bytes();
                let mut temp = Self::from_reader(&mut string)?;
                std::mem::swap(&mut temp, self);
            },
            _ => ()
        }
        Ok(())
    }

    // pub fn resolve_deep(&mut self) -> Result<(), XmlError> {
    //     match self {
    //         Self::None => Ok(()),
    //         Self::XmlNodes(nodes) => {
    //             for node in nodes {
    //                 node.resolve_child_nodes()
    //             }
    //         },
    //         Self::UnParsed(_) => {
    //             self.resolve()?;
    //             self.resolve_deep()
    //         }
    //     }
    //     Ok(())
    // }

    pub fn nodes_resolved(&mut self) -> Result<&mut [Node], XmlError> {
        match self {
            Self::None => Ok(&mut []),
            Self::XmlNodes(nodes) => Ok(nodes),
            Self::UnParsed(_) => {
                self.resolve()?;
                self.nodes_resolved()
            }
        }
    }

    pub fn into_nodes(mut self) -> Result<Vec<Node>, XmlError> {
        match self {
            Self::None => Ok(vec![]),
            Self::XmlNodes(nodes) => Ok(nodes),
            Self::UnParsed(_) => {
                self.resolve()?;
                self.into_nodes()
            }
        }
    }
}

impl fmt::Display for NodeList {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        let tab = "\t".repeat(width);
        let child_nodes = match &self {
            NodeList::XmlNodes(nodes) => {
                let separator = format!("\n{}", tab);
                let children = nodes.iter().map(|attr|attr.to_string()).collect::<Vec<String>>();
                children.join(&separator)
            },
            NodeList::UnParsed(string) => string.clone(),
            NodeList::None => "".to_string(),
        };
        write!(f, "{}", child_nodes)
    }
}

impl fmt::Debug for NodeList {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_child_nodes_resolve_unparsed() -> Result<(), XmlError> {
        let string = "<a /><b /><c at='a1'><d /><e /></c>";
        let mut unparsed = NodeList::UnParsed(string.to_string());
        let nodes = unparsed.nodes_resolved()?;

        assert_eq!(nodes.len(), 3);
        assert_eq!(&nodes[0].to_string(), "<a />");
        assert_eq!(&nodes[1].to_string(), "<b />");
        if let Node::N {tag, attributes, child_nodes} = &nodes[2] {
            assert_eq!(tag, "c");
            let attr = attributes.get("at");
            assert!(matches!(attr, Some(_)));
            assert_eq!(attr.unwrap(), "a1");
            assert!(matches!(child_nodes, NodeList::UnParsed(_)));
            if let NodeList::UnParsed(nodes) = child_nodes {
                assert_eq!(nodes, "<d /><e />");
            }
            else {
                unreachable!();
            }
        }
        else {
            unreachable!();
        }
        Ok(())
    }
}

