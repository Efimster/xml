use std::error::Error;
use std::{fmt, io};
use std::num::ParseIntError;
use std::str::Utf8Error;
use std::string::FromUtf8Error;

#[derive(Debug)]
pub enum XmlErrorKind {
    Parse,
    ParseString,
    ParseNumber,
    IOEof,
    Inner
}

#[derive(Debug)]
pub struct XmlError {
    pub source:Option<Box<dyn Error>>,
    pub kind:XmlErrorKind,
}

impl XmlError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        XmlError {
            source: Some(source),
            kind: XmlErrorKind::Inner
        }
    }

    pub fn new_kind(kind:XmlErrorKind)->Self{
        XmlError {
            source: None,
            kind
        }
    }
}

impl Error for XmlError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl XmlErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:XmlError = self.into();
        error.into()
    }
}

impl fmt::Display for XmlErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            XmlErrorKind::Parse => write!(f, "faile parse"),
            XmlErrorKind::ParseString => write!(f, "faile parse string"),
            XmlErrorKind::ParseNumber => write!(f, "faile parse number"),
            XmlErrorKind::IOEof => write!(f, "end of file"),
            XmlErrorKind::Inner =>  write!(f, "error in XmlError::source"),
        }
    }
}

impl fmt::Display for XmlError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<XmlErrorKind> for XmlError {
    fn from(kind: XmlErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for XmlError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl XmlError {
    pub fn is_eof(&self) -> bool{
        if matches!(self.kind, XmlErrorKind::IOEof) {return true}
        if !matches!(self.kind, XmlErrorKind::Inner) {return false}
        if let Some(error) = self.source().unwrap().downcast_ref::<io::Error>() {
            return matches!(error.kind(), io::ErrorKind::UnexpectedEof);
        }
        false
    }
}

impl From<Utf8Error> for XmlError {
    fn from(error: Utf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<FromUtf8Error> for XmlError {
    fn from(error: FromUtf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<ParseIntError> for XmlError {
    fn from(error: ParseIntError) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<io::Error> for XmlError {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}

