use crate::structs::attribute::Attribute;
use crate::enums::node_list::NodeList;
use core::{fmt, str};
use std::cmp;
use std::fmt::{Formatter, Display};
use std::io::{BufRead, ErrorKind};
use crate::enums::xml_error::XmlError;
use crate::utils::constants::{CDATA_END, CDATA_END_BYTES, CDATA_TAG, CDATA_TAG_BYTES, COMMENT_END, COMMENT_END_BYTES, COMMENT_TAG, COMMENT_TAG_BYTES, LT, P_INSTRUCTIONS_END_BYTES, RT, SLASH, TAG_SEPARATORS_SET};
use crate::utils::attribute_util;
use crate::structs::attribute_list::AttributeList;
use slib::utils::parse_util::{self, read_until_any_pattern, read_until_pattern};

use super::xml_error::XmlErrorKind;

#[derive(PartialEq)]
pub enum Node {
    CData(String),
    Text(String),
    Comment(String),
    PInstructions(String),
    Meta{tag:String, attributes:AttributeList},
    N{tag:String, attributes:AttributeList, child_nodes: NodeList},
    None,
}

impl Node {
    pub fn new(tag:impl ToString, attributes:Vec<Attribute>, child_nodes:NodeList) -> Node {
        Node::N {
            tag:tag.to_string(),
            attributes:attributes.into(),
            child_nodes,
        }
    }

    pub fn new_cdata(text:impl ToString) -> Node {
        Node::CData(text.to_string())
    }

    pub fn new_comment(text:impl ToString) -> Node {
        Node::Comment(text.to_string())
    }

    pub fn new_text(text:impl ToString) -> Node {
        Node::Text(text.to_string())
    }

    pub fn new_processing_instructions(text:impl ToString) -> Node {
        Node::PInstructions(text.to_string())
    }

    pub fn new_meta(tag:impl ToString, attributes:Vec<Attribute>) -> Node {
        Node::Meta {
            tag:tag.to_string(),
            attributes:attributes.into(),
        }
    }

    pub fn from_reader(reader:&mut impl BufRead) -> Result<Node, XmlError> {
        Self::read_until_node(reader)
    }

    pub fn read_until_node(reader:&mut impl BufRead) -> Result<Node, XmlError> {
        let (tag, mut to) = Self::read_until_tag(reader)?;
        if let None = tag {
            let text = std::str::from_utf8(&to)?.trim();
            return if text.len() > 0 {    
                Ok(Node::new_text(text))
            } else {
                Err(XmlErrorKind::IOEof.into())
            };
        }
        let tag = tag.unwrap();
        if tag == "!" {
            Self::read_meta_node(reader)
        } else if tag == "?" {
            Self::read_instructions_node(reader)
        } else {
            Self::read_regular_node(reader, tag, &mut to)
        }
    }

    fn read_until_tag(reader:&mut impl BufRead) -> Result<(Option<String>,Vec<u8>), XmlError> {
        let mut to:Vec<u8> = Vec::new();
        let len_to_lt = reader.read_until(LT, &mut to)?;
        if to.len() == 0 {
            return Err(Into::<std::io::Error>::into(ErrorKind::UnexpectedEof).into());
        } else if to[to.len() - 1] != LT {
            return Ok((None, to));
        }
        
        let (_, len) = parse_util::read_until_any(reader, TAG_SEPARATORS_SET, &mut to)?;
        let mut tag = &to[len_to_lt .. len_to_lt + len];
        if len > 1 {
            tag = &tag[.. tag.len() -1];
        }
        let tag = std::str::from_utf8(tag)?;

        Ok((Some(tag.to_string()), to))
    }

    fn read_instructions_node(reader:&mut impl BufRead) -> Result<Node, XmlError>{
        let mut to = vec![];
        read_until_pattern(reader, P_INSTRUCTIONS_END_BYTES, &mut to)?;
        Ok(Node::new_processing_instructions(std::str::from_utf8(&to[.. to.len() - P_INSTRUCTIONS_END_BYTES.len()])?))
    }

    fn read_meta_node(reader:&mut impl BufRead) -> Result<Node, XmlError>{
        let mut to = vec![];
        reader.read_until(RT, &mut to)?;
        let mut attributes_buffer = &to[ .. to.len() - 1];
        let mut attributes = attribute_util::parse_attributes(&mut attributes_buffer)?;
        let tag = attributes.remove(0).name;
        if tag.starts_with(&CDATA_TAG[1..]) {
            Ok(Node::new_cdata(std::str::from_utf8(&to[CDATA_TAG_BYTES.len() - 1 .. to.len() - CDATA_END_BYTES.len()])?))
        } else if tag.starts_with(&COMMENT_TAG[1..]) {
            Ok(Node::new_comment(std::str::from_utf8(&to[COMMENT_TAG_BYTES.len() - 1  .. to.len() - COMMENT_END_BYTES.len()])?))
        } else {
            Ok(Node::new_meta(tag, attributes))
        }
    }

    fn read_regular_node(reader:&mut impl BufRead, tag:String, to:&mut Vec<u8>) -> Result<Node, XmlError>{
        let tag_len = to.len();
        if to[to.len() - 1] != RT {
            reader.read_until(RT, to)?;
        };

        let is_simple = to.len() > 1 && to[to.len() - 2] == SLASH;
        let mut attributes_buffer = &to[tag_len .. cmp::max(tag_len, to.len() - 1 - is_simple as usize)]; 
        let attributes = attribute_util::parse_attributes(&mut attributes_buffer)?;
        to.truncate(0);
        if is_simple {
            return Ok(Node::new(tag, attributes, NodeList::None));
        }

        let open_pattern = format!("<{}", tag).into_bytes();
        let close_pattern = format!("</{}>", tag).into_bytes();
        let mut close_patterns_to_find = 1;
        let check_set:&[&[u8]] = &[&close_pattern, &open_pattern];
        while close_patterns_to_find > 0 {
            let (pattern, _size) = read_until_any_pattern(reader, check_set, to)?;
            if pattern == open_pattern {
                to.push(0);
                let last_index = to.len() - 1;
                reader.read_exact(&mut to[last_index ..])?;
                if !TAG_SEPARATORS_SET.contains(&to[last_index]) {
                    continue;
                }
                close_patterns_to_find += 1;
            } else {
                close_patterns_to_find -= 1;
            }
        }
        
        let inner_string = std::str::from_utf8(&to[0 .. to.len() - close_pattern.len()])?;
        Ok(Node::new(tag, attributes, NodeList::UnParsed(inner_string.to_string())))
    }



    pub fn take(&mut self) -> Self {
        let mut result = Node::None;
        std::mem::swap(&mut result, self);
        result
    }

    pub fn tag (&self) -> Option<&str> {
        match self {
            Node::N {tag, attributes:_, child_nodes:_ } => Some(tag),
            Node::Meta { tag, attributes:_ } => Some(tag),
            _ => None,
        }
    }

    pub fn resolve_child_nodes(&mut self) -> Result<Option<&mut[Node]>, XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(Some(child_nodes.nodes_resolved()?));
        }
        Ok(None)
    }

    pub fn get_child_nodes(&self) -> Result<Option<&[Node]>, XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(Some(child_nodes.nodes()));
        }
        Ok(None)
    }

    pub fn get_child_nodes_mut(&mut self) -> Result<Option<&mut [Node]>, XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(Some(child_nodes.nodes_mut()));
        }
        Ok(None)
    }

    pub fn get_child_node_list(&self) -> Result<Option<&NodeList>, XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(Some(child_nodes));
        }
        Ok(None)
    }

    pub fn resolve_child_nodes_deep(&mut self) -> Result<(), XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            child_nodes.resolve()?;
            match child_nodes {
                NodeList::None => (),
                NodeList::XmlNodes(nodes) => {
                    for node in nodes {
                        node.resolve_child_nodes_deep()?;
                    }
                },
                NodeList::UnParsed(_) => unreachable!(),
            }
            return Ok(());
        }
        Ok(())
    }

    pub fn tag_and_attributes(&self) -> Option<(&str, &AttributeList)> {
        match self {
            Node::N {tag, attributes, child_nodes:_ } => {
                Some((tag, attributes))
            },
            _ => None,
        }
    }

    pub fn attributes(&self) -> Option<&AttributeList> {
        match self {
            Node::N {tag:_, attributes, child_nodes:_ } => {
                Some(attributes)
            },
            _ => None,
        }
    }

    pub fn child_nodes_len(&self) -> Result<usize, XmlError> {
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(child_nodes.nodes().len());
        }
        Ok(0)
    }

    pub fn into_child_node_list(self) -> Result<Option<NodeList>, XmlError>{
        if let Self::N {tag:_, attributes:_, child_nodes} = self {
            return Ok(Some(child_nodes));
        }
        Ok(None)
    }

    // pub fn params(&self) -> Option<(&str, &AttributeList, )> {
    //     match self {
    //         Node::N {tag, attributes, child_nodes:_ } => {
    //             Some((tag, attributes))
    //         },
    //         _ => None,
    //     }
    // }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let width = f.width().unwrap_or_default();
        let tab = "\t".repeat(width);
        match self {
            Node::Comment(text) => write!(f, "{}<!--{}{}", tab, text, COMMENT_END),
            Node::CData(text) => write!(f, "{}<{}{}{}", tab, CDATA_TAG, text, CDATA_END),
            Node::PInstructions(text) => write!(f, "{}<?{}?>", tab, text),
            Node::Text(text) => write!(f, "{}", text),
            Node::N {tag, attributes, child_nodes} => {
                let attributes = attributes.iter()
                    .map(|(key, value)|attribute_util::to_attribute_string(key, value)).collect::<Vec<String>>();
                let attributes:String = attributes.join(" ");
                let children = format!("{:<width$}", child_nodes, width = width + 1);
                if children.len() > 0 {
                    write!(f, "{}<{} {}>\n{}\t{}\n</{}>", tab, tag, attributes, tab, children, tag)
                }
                else {
                    write!(f, "<{} {}/>", tag, attributes)
                }
            },
            Node::Meta {tag, attributes} => {
                let attributes = attributes.iter()
                    .map(|(key, value)|attribute_util::to_attribute_string(key, value)).collect::<Vec<String>>();
                let attributes:String = attributes.join(" ");
                write!(f, "<{} {}/>", tag, attributes)
            },
            Node::None => Ok(())
        }
    }
}

impl fmt::Debug for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_until_tag() -> Result<(), XmlError> {
        assert_eq!(Node::read_until_tag(&mut "bla <tag>".as_bytes())?.0.unwrap(), "tag");
        assert_eq!(Node::read_until_tag(&mut r#"bla <tag />"#.as_bytes())?.0.unwrap(), "tag");
        assert_eq!(Node::read_until_tag(&mut r##"bla <tag class="1" />"##.as_bytes())?.0.unwrap(), "tag");
        assert_eq!(Node::read_until_tag(&mut r##"bla <tag class="1">"##.as_bytes())?.0.unwrap(), "tag");
        assert_eq!(Node::read_until_tag(&mut r##"b <!Doctype>"##.as_bytes())?.0.unwrap(), "!");
        assert_eq!(Node::read_until_tag(&mut r##"b <![CDATA[ "##.as_bytes())?.0.unwrap(), "!");
        assert_eq!(Node::read_until_tag(&mut r##"b <!----> "##.as_bytes())?.0.unwrap(), "!");
        assert_eq!(Node::read_until_tag(&mut r##"b <??> "##.as_bytes())?.0.unwrap(), "?");
        assert!(matches!(Node::read_until_tag(&mut r##"bla <tag"##.as_bytes()), Err(_)));
        Ok(())
    }

    #[test]
    fn test_read_until_comment_node() -> Result<(), XmlError> {
        assert_eq!(Node::read_until_node(&mut "<!--Copyright-->".as_bytes())?, Node::Comment("Copyright".to_string()));
        Ok(())
    }

    #[test]
    fn test_read_until_cdata_node() -> Result<(), XmlError> {
        assert_eq!(Node::read_until_node(&mut "<![CDATA['Data']]>".as_bytes())?, Node::CData("'Data'".to_string()));
        Ok(())
    }

    #[test]
    fn test_read_until_instructions_node() -> Result<(), XmlError> {
        let string = r#"<?xml version="1.0" encoding="us-ascii" standalone="yes"?>"#;
        assert_eq!(Node::read_until_node(&mut string.as_bytes())?, 
            Node::PInstructions(r#"xml version="1.0" encoding="us-ascii" standalone="yes""#.to_string()));
        Ok(())
    }

    #[test]
    fn test_read_until_meta_node() -> Result<(), XmlError> {
        let attribute = "html".parse()?;
        assert_eq!(Node::read_until_node(&mut "<!DOCTYPE html>".as_bytes())?, 
            Node::Meta{tag:"DOCTYPE".to_string(), attributes:[attribute].into()});
        Ok(())
    }

    #[test]
    fn test_node_from_reader_text() -> Result<(), XmlError> {
        assert_eq!(Node::read_until_node(&mut "this is some text".as_bytes())?, Node::Text( "this is some text".to_string()));
        Ok(())
    }

    #[test]
    fn test_read_until_regular_node() -> Result<(), XmlError> {
        let string = r##"<div class="1" >
            <div class="2">
                <div class="3">
                    <div class="4"></div>
                </div>
            </div>
            <div class="5"></div>
        </div>
        "##;
        let xml = Node::read_until_node(&mut string.as_bytes())?;
        assert_eq!(xml.tag().unwrap(), "div");
        assert_eq!(xml.attributes().unwrap().get("class").unwrap(), "1");
        let xml = xml.into_child_node_list()?.unwrap().into_nodes()?;
        assert_eq!(xml.len(), 2);

        Ok(())
    }

    #[test]
    fn test_node_from_reader_1() -> Result<(), XmlError> {
        // let string = " <link:footnoteArc  xlink:arcrole=\"http://www.xbrl.org/2003/arcrole/fact-footnote\" \
        // xlink:from=\"F_000343\" \
        // xlink:to=\"FNT_000001\" \
        // xlink:type=\"arc\"  />";
        // let node = Node::from_reader(&mut string.as_bytes())?;
        // match node {
        //     Node::N {tag, attributes, child_nodes} => {
        //         assert_eq!(&tag, "link:footnoteArc");
        //         assert_eq!(attributes.len(), 4);
        //         assert!(matches!(child_nodes, NodeList::None));
        //     },
        //     _ => panic!("test failed, wrong node type"),
        // }

        let string = " <link>\
            <a />\
            <b></b>\
        </link>";
        let node = Node::from_reader(&mut string.as_bytes())?;
        match node {
            Node::N {tag, attributes, child_nodes} => {
                assert_eq!(tag, "link");
                assert_eq!(attributes.len(), 0);
                match child_nodes {
                    NodeList::UnParsed(string) => {
                        assert_eq!(&string, "<a /><b></b>");
                    },
                    _ => return Err(XmlErrorKind::ParseString.into()),
                }
            },
            _ => panic!("test failed, wrong node type"),
        }
        Ok(())
    }



    #[test]
    fn test_node_resolve_child_nodes_deep() -> Result<(), XmlError> {
        let string = "<unit id=\"u6\">
            <divide>
                <unitNumerator>
                    <measure>ISO4217:EUR</measure>
                </unitNumerator>
                <unitDenominator>
                    <measure>xbrli:shares</measure>
                </unitDenominator>
            </divide>
        </unit>";
        let mut unit = Node::from_reader(&mut string.as_bytes())?;
        unit.resolve_child_nodes_deep()?;
        let nodes = unit.get_child_nodes()?.unwrap();
        assert_eq!(nodes.len(), 1);
        let divide = &nodes[0];
        let nodes = divide.get_child_nodes()?.unwrap();
        assert_eq!(nodes.len(), 2);
        let unit_numerator = &nodes[0];
        let unit_denominator = &nodes[1];
        let nodes = unit_numerator.get_child_nodes()?.unwrap();
        assert_eq!(nodes.len(), 1);
        let nodes = unit_denominator.get_child_nodes()?.unwrap();
        assert_eq!(nodes.len(), 1);
        Ok(())
    }

    #[test]
    fn test_node_from_reader_2() -> Result<(), XmlError> {
        let string = r##"<u:n
          contextRef="C_0001318605_us-gaapFiniteLivedIntangibleAssetsByMajorClassAxis_us-gaapDevelopedTechnologyRightsMember_20191231"
          decimals="-6"
          id="F_000906"
          unitRef="U_iso4217USD">72000000</u:n>"##;
        Node::from_reader(&mut string.as_bytes())?;
        let string = "<ProcessingTime/>";
        Node::from_reader(&mut string.as_bytes())?;

        Ok(())
    }
}